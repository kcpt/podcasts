<?php
/**
 * Template Name: Podcast XML
 * Description: Podcaster XML
 *
 * @package WP-Bootstrap
 * @subpackage Default_Theme
 * @since WP-Bootstrap 0.5
 *
 */

/*** STARTER VARIABLES ***
---------------------------------------------*/
$xml 			= isset( $_REQUEST['xml'] ) ? $_REQUEST['xml'] : false; // if xml
$slug 			= isset( $_REQUEST['slug'] ) ? $_REQUEST['slug'] : false; // Slug name
$invalidSlug 	= false; // For logging use

// Check if slug exists
// --- Bad slug returns null
// --- No slug returns false
$slugObj = $slug ? get_page_by_path( $slug ) : false;

if( !$xml ):

	/*********************************
	DISPLAY PODCAST INFO
	( HEADER AND FOOTER DISPLAYED - NO XML )
	*********************************/

	get_header();
	?>

	<style>
		h1.maintitle {
			font-size: 24px;
			margin-bottom: 20px;
		}
		.theimage {
		  	margin-bottom: 20px;
		}
		.theimage img {
		  	max-width: 100%;
		}
		.thelinks {
			padding-left: 15px;
			margin-bottom: 20px;
		}
		h2.podcast-title {
		  	font-size: 20px;
		}
		.bktopodcasts {
		  	margin: 20px 0 10px;
		}
		.bktopodcasts span {
		  	font-size: 14px;
		  	margin-right: 10px;
		  	font-weight: bold;
		  	position: relative;
		  	top: -1px;
		}
	</style>

	<?php 
	if( $slugObj ): // IF EXISTING SLUG QUERY

		$slugID = $slugObj->ID;

		$args = array(
		'page_id'    => $slugID,
		'meta_key'   => 'podcastxml_enabled',
		'meta_value' => true
		);

		// The Query
		$the_query = new WP_Query( $args );

	else: // IF SLUG INVALID OR EMPTY, QUERY ALL PODCASTS

		$args = array(
		'post_type'  => 'page',
		'meta_key'   => 'podcastxml_enabled',
		'meta_value' => true
		);
		// The Query
		$the_query = new WP_Query( $args );

		if( $slugObj === null ):
			$invalidSlug = true;
		endif;

	endif;

	if( $invalidSlug ): ?>

		<h1 class="invalid-slug">Hmmm... Your slug seems to be invalid. Please double check the spelling.</h1>

	<?php endif;

	$mainTitle = $slugObj ? 'Podcasts for '.ucwords(str_replace('-', ' ', $slug)) : 'All Podcasts';

	echo '<h1 class="maintitle">Showing '.$mainTitle.'</h1>';	

	// The Loop
	if ( $the_query->have_posts() ):

		while ( $the_query->have_posts() ) :
			$the_query->the_post();

			$podcastxml_title 			= get_field('podcastxml_title');
			$podcastxml_subtitle 		= get_field('podcastxml_subtitle');
			$podcastxml_copyright 		= get_field('podcastxml_copyright');
			$podcastxml_author 			= get_field('podcastxml_author');
			$podcastxml_description 	= get_field('podcastxml_description');
			$podcastxml_owner 			= get_field('podcastxml_owner');
			$podcastxml_owner_email 	= get_field('podcastxml_owner_email');
			$podcastxml_main_image 		= get_field('podcastxml_main_image');
			$podcastxml_categories 		= get_field('podcastxml_categories');
			$podcastxml_add_podcast 	= get_field('podcastxml_add_podcast');

			?>

			<div class="thepodcasts row">

				<div class="theimage col-xs-1">
					<a href="<?= get_the_permalink(); ?>"><img src="<?= $podcastxml_main_image; ?>"/></a>
				</div>

				<div class="thelinks col-xs-11">

					<h2 class="podcast-title">
						<a href="<?= get_the_permalink();?>"><?= $podcastxml_title; ?></a>
						<a href="<?php bloginfo('url');?>/podcasts/?slug=<?= $post->post_name; ?>&xml=true" style="font-size:12px;display:inline-block;margin-left:5px;position:relative;top:-2px;">[ View XML ]</a>
					</h2>
					
					<?php // LIST OF ALL EPISODE PODCASTS
					if( have_rows('podcastxml_add_podcast') ):
					$podcastCount = 0;
					while ( have_rows('podcastxml_add_podcast') ): the_row();
						$podcastCount++;
						if( !$slug && $podcastCount < 5 ) { // Limiter
							echo '<h3>'.get_sub_field('podcastxml_item_title').'</h3>';
						} elseif( $slug ) {
							echo '<h3>'.get_sub_field('podcastxml_item_title').'</h3>';
						}
					endwhile;
					endif;
					
					if( !$slug && $podcastCount >= 5 ) {
						echo '<a href="' . get_bloginfo('url') . '/podcasts/?slug=' . $post->post_name . '" style="font-size:12px;padding:3px 10px;display:inline-block;background-color:#21759B;color:white;border-radius:5px;margin-top:5px;">' . ($podcastCount-4) . ' more</a>';
					}

					?>

				</div>

			</div><!-- /.row -->
			<?php

		endwhile;

		if( $slugObj ):
			echo '<a class="bktopodcasts btn btn-large btn-info" href="'.get_bloginfo('url').'/podcasts"><span>‹</span>Back to All Podcasts</a>';
		endif;

	else:
		// no posts found
		echo '<h1 class="maintitle">Sorry, there are no podcasts for the page you provided.</h1>';
		echo '<p>Hint: Try another page in the url above, or...</p>';
		echo '<a class="bktopodcasts btn btn-large btn-info" href="'.get_bloginfo('url').'/podcasts"><span>‹</span>Back to All Podcasts</a>';
	endif;
	/* Restore original Post Data */
	wp_reset_postdata();

	get_footer();

else:

	/*********************************
	DISPLAY XML 
	( HEADER AND FOOTER DISPLAYED IF XML INVALID )
	*********************************/

	if( $slug && $slugObj ): // IF SLUG VALID

		$slugID = $slugObj->ID;
		$args = array(
		'page_id'    => $slugID,
		'meta_key'   => 'podcastxml_enabled',
		'meta_value' => true
		);

		// The Query
		$the_query = new WP_Query( $args );

		if ( $the_query->have_posts() ):

			/*----------
			****** ALL XML PARAMETERS ARE CORRECT *****
			-----------------------------------------*/
			include('include-podcast/podcast-xml.php');

		else:

			/*----------
			XML INVALID CONTAINS NO PODCASTS
			------------------------------*/
			get_header();
			?>

			<style>
				h1.maintitle {
					font-size: 24px;
					margin-bottom: 20px;
				}
				.theimage {
				  	margin-bottom: 20px;
				}
				.theimage img {
				  	max-width: 100%;
				}
				.thelinks {
					padding-left: 15px;
					margin-bottom: 20px;
				}
				h2.podcast-title {
				  	font-size: 20px;
				}
				.bktopodcasts {
				  	margin: 20px 0 10px;
				}
				.bktopodcasts span {
				  	font-size: 14px;
				  	margin-right: 10px;
				  	font-weight: bold;
				  	position: relative;
				  	top: -1px;
				}
			</style>



			<?php
			echo '<h1 class="maintitle">Sorry, there are no podcasts for the page you provided.</h1>';
			echo '<p>Hint: Try another page in the url above, or...</p>';
			echo '<a class="bktopodcasts btn btn-large btn-info" href="'.get_bloginfo('url').'/podcasts"><span>‹</span>Back to All Podcasts</a>';
			get_footer();

		endif;
		wp_reset_postdata();

	else:

		/*----------
		SLUG INVALID OR DOESN'T EXIST
		------------------------------*/
		get_header();
		if( $slugObj === null ):
			$invalidSlug = true;
		endif;

		if( $invalidSlug ): ?>

			<h1 class="invalid-slug">Hmmm... Your slug seems to be invalid. Please double check the spelling.</h1>

		<?php endif; ?>

		<style>
			h1.maintitle {
				font-size: 24px;
				margin-bottom: 20px;
			}
			.theimage {
			  	margin-bottom: 20px;
			}
			.theimage img {
			  	max-width: 100%;
			}
			.thelinks {
				padding-left: 15px;
				margin-bottom: 20px;
			}
			h2.podcast-title {
			  	font-size: 20px;
			}
			.bktopodcasts {
			  	margin: 20px 0 10px;
			}
			.bktopodcasts span {
			  	font-size: 14px;
			  	margin-right: 10px;
			  	font-weight: bold;
			  	position: relative;
			  	top: -1px;
			}
		</style>



		<?php
		echo '<h1 class="maintitle">Hmmm... Something is not quite right. Please use a valid page name</h1>';
		echo '<p>Hint: Add a page name in the url above, or...</p>';
		echo '<a class="bktopodcasts btn btn-large btn-info" href="'.get_bloginfo('url').'/podcasts"><span>‹</span>Back to All Podcasts</a>';
		get_footer();

	endif;

endif;

