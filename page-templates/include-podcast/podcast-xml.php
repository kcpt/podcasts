<?php
$podcastxml_title 			= htmlspecialchars(get_field('podcastxml_title', $slugID));
$podcastxml_subtitle 		= htmlspecialchars(get_field('podcastxml_subtitle', $slugID));
$podcastxml_copyright 		= htmlspecialchars(get_field('podcastxml_copyright', $slugID));
$podcastxml_author 			= htmlspecialchars(get_field('podcastxml_author', $slugID));
$podcastxml_description 	= htmlspecialchars(get_field('podcastxml_description', $slugID));
$podcastxml_owner 			= htmlspecialchars(get_field('podcastxml_owner', $slugID));
$podcastxml_owner_email 	= htmlspecialchars(get_field('podcastxml_owner_email', $slugID));
$podcastxml_main_image 		= htmlspecialchars(get_field('podcastxml_main_image', $slugID));
$podcastxml_categories 		= get_field('podcastxml_categories', $slugID);
$podcastxml_add_podcast 	= get_field('podcastxml_add_podcast', $slugID);

header('Content-Type: application/xml; charset=utf-8');

echo '<?xml version="1.0" encoding="utf-8" ?>';

// function safeLink($link) {
// 	$link = preg_replace("/(&lt;a.*&lt;\/a&gt;)/", "<![CDATA['$1']]>", $link);
// 	return $link;
// }

?>

<rss xmlns:itunes="http://www.itunes.com/dtds/podcast-1.0.dtd" version="2.0">

<channel>

<title><?= $podcastxml_title; ?></title>

<link><?php the_permalink(); ?></link>

<language>en-us</language>

<copyright><?= $podcastxml_copyright; ?></copyright>

<itunes:subtitle><?= $podcastxml_subtitle; ?></itunes:subtitle>

<itunes:author><?= $podcastxml_author; ?></itunes:author>

<itunes:summary><?= $podcastxml_description; ?></itunes:summary>

<description><?= $podcastxml_description; ?></description>

<itunes:owner>

<itunes:name><?= $podcastxml_owner; ?></itunes:name>

<itunes:email><?= $podcastxml_owner_email; ?></itunes:email>

</itunes:owner>

<itunes:image href="<?= $podcastxml_main_image; ?>" />

<?php // iTUNES CATEGORIES
$categories = preg_split('/<br[^>]*>/i', $podcastxml_categories);
//print_r($categories);
foreach( $categories as $category ) {
	
	$c = explode(",", $category);
	if ( count($c) > 1 ) {
		//echo $c[0].$c[1];
		echo "<itunes:category text='".trim(htmlspecialchars($c[0]))."'>";
		echo "<itunes:category text='".trim(htmlspecialchars($c[1]))."'/>";
		echo "</itunes:category>";
	} else {
		// echo $c[0];
		echo "<itunes:category text='".trim(htmlspecialchars($c[0]))."'/>";
	}
}
// END CATEGORIES

// PODCAST WHILE LOOP
if( have_rows('podcastxml_add_podcast', $slugID) ):
while ( have_rows('podcastxml_add_podcast', $slugID) ): the_row();

$podcastxml_item_title 			= htmlspecialchars(get_sub_field('podcastxml_item_title'));
$podcastxml_item_main_author 	= get_sub_field('podcastxml_item_main_author');

if ( $podcastxml_item_main_author == '1' ):
	$podcastxml_item_author 	= htmlspecialchars(get_sub_field('podcastxml_item_author'));
else:
	$podcastxml_item_author 	= htmlspecialchars($podcastxml_author);
endif;

$podcastxml_item_subtitle 		= htmlspecialchars(get_sub_field('podcastxml_item_subtitle'));
$podcastxml_item_summary 		= htmlspecialchars(get_sub_field('podcastxml_item_summary'));
$podcastxml_item_main_image 	= get_sub_field('podcastxml_item_main_image');

if ( $podcastxml_item_main_image == '1' ):
	$podcastxml_item_image 		= get_sub_field('podcastxml_item_image');
else:
	$podcastxml_item_image 		= $podcastxml_main_image;
endif;

// TO DO --- UPDATE PODCAST TYPE BY PHP TRIM INSTEAD OF ACF
$podcastxml_item_type 			= get_sub_field('podcastxml_item_type');
switch ( $podcastxml_item_type ) {
	case 'm4a':
		$podcastxml_item_type = 'audio/mpeg';
		break;
	case 'mp3':
		$podcastxml_item_type = 'audio/x-m4a';
		break;
	case 'mov':
		$podcastxml_item_type = 'video/mp4';
		break;
	case 'mp4':
		$podcastxml_item_type = 'video/x-m4v';
		break;
	case 'm4v':
		$podcastxml_item_type = 'video/quicktime';
		break;
	case 'pdf':
		$podcastxml_item_type = 'application/pdf';
		break;
	case 'epub':
		$podcastxml_item_type = 'document/x-epub';
		break;
}
$podcastxml_item_podcast 		= get_sub_field('podcastxml_item_podcast');
$podcastxml_item_publish_date 	= get_sub_field('podcastxml_item_publish_date');
$podcastxml_item_duration 		= get_sub_field('podcastxml_item_duration');

?>

<item>

<title><?= $podcastxml_item_title; ?></title>

<itunes:author><?= $podcastxml_item_author; ?></itunes:author>

<itunes:subtitle><?= $podcastxml_item_subtitle; ?></itunes:subtitle>

<itunes:summary><?= $podcastxml_item_summary; ?></itunes:summary>

<itunes:image href='<?= $podcastxml_item_image; ?>' />

<enclosure url='<?= $podcastxml_item_podcast['url']; ?>' length='<?= filesize( get_attached_file( $podcastxml_item_podcast['id'] ) ); ?>' type='<?= $podcastxml_item_type; ?>' />

<guid><?= $podcastxml_item_podcast['url']; ?></guid>

<?php /* MAKE SURE TIME IS CORRECT */ ?>
<pubDate><?= date( 'D, d M Y H:i:s', strtotime( $podcastxml_item_publish_date ) ); ?> CDT</pubDate>

<itunes:duration><?= $podcastxml_item_duration ?></itunes:duration>

</item>

<?php

endwhile;
endif;
// END WHILE LOOP

?>

</channel>

</rss>